# implement Fresnel's equations for a thin film and some utility functions
import cmath 
def degreestoradians(degrees):
	return (pi*degrees/180.0)
	
def cos_snell(n1,n2,angle):
#uses a combination of Snell's law and 1=cos^2+sin^2 to calculate the cosine of the incident angle in the given media
	return(cmath.sqrt(1.0-(cmath.sin(angle)*(n1/n2))**2.));


def rp( n1, n2,  cangle1,  cangle2):

	# calculates the fresnel term rp
	# the cangle variables are the cosines of the angles.
	return ((n2*cangle1-n1*cangle2)/(n2*cangle1+n1*cangle2))
	
def rs(n1,n2,cangle1,cangle2):

	# calculates the fresnel term rs
	# the cangle variables are the cosines of the angles.
	return ((n1*cangle1-n2*cangle2)/(n1*cangle1+n2*cangle2))
