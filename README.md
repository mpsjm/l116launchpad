# L116Launchpad README

This project aims to control a Gaernter L116 ellipsometer using a TI Launchpad development board.

## Some background ##

I have an old (20+ year old) L116 ellipsometer. Its computer interface has failed and the motor that drives its analyser has failed. This project aims to replace the computer interface with a TI Launchpad powered by a MSP 430 chip. Fortunately most of the electrical signals are available via a set of connectors on the side of the machine. 

A proof of principle system based on a LabJack data acquistion system and coded in Python has been set up. This project aims to build on that and provide a stand alone piece of hardware. It will have the added benefit of using 12-bit ADC -- the LabJack is a 10 bit system.

In the short term that motor problem can be sidestepped by manually rotating the polariser. In a further iteration of the project a motor drive will be developed. This will probably be stepper motor controlled.


